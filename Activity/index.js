// console.log("Hello World!");

console.log("Original Array: ");

let firstArray = ['Dwayne Johnson', 'Steve Austin', 'Kurt Angle', 'Dave Bautista'];

console.log(firstArray);

function addArray (add) {
	firstArray[firstArray.length] = add;
	console.log(firstArray);
}

let addOfArray = addArray('John Cena');

let itemFound = [];

function addIndex (index) {
	return itemFound[index];
}

let adIn = addIndex[0];
adIn = 'Kurt Angle';

console.log(adIn);


function deleteArray () {
	let storeArray = firstArray[firstArray.length-1];
	firstArray.length--;
	return storeArray;
}

let delArray = deleteArray();
console.log(delArray);
console.log(firstArray);

function updateArray (update, index) {
	firstArray[index] = update;
}

updateArray('Triple H', 3);
console.log(firstArray);

function removeAll () {
	firstArray.length = 0;
}

removeAll();
console.log(firstArray);

function checkIfEmpty () {
	if (firstArray.length > 0) {
		console.log(false);
	} else {
		console.log(true);
	}
}

let isEmpty = checkIfEmpty();